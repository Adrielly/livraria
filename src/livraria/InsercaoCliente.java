
package livraria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class InsercaoCliente {
 public void inserirCliente (String nome, 
         String telefone, String CPF, String RG, String email) {
     try {
         Connection c = Conexao. ObterConexao();
         PreparedStatement ps = c.prepareStatement ("insert into sc_adrielly.cliente(nome_cli, email_cli, cpf_cli, telefone_cli, rg_cli) values (?,?)");
         ps.setString(1,nome);
         ps.setString(2, email);
         ps.setString(3, telefone);
         ps.setString(4, CPF);
         ps.setString(5, RG);
         ps.executeUpdate();
         c.close();
     } catch (SQLException ex) {
         Logger.getLogger(InsercaoCliente.class.getName()).log(Level.SEVERE, null, ex);
     }
        
     
 }
}
