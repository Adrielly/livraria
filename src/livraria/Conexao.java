
package livraria;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Conexao {
    public static Connection ObterConexao () {
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "adrielly";
            String senha = "thehobbit";
            String banco = "jdbc:postgresql://10.90.24.54/adrielly";
            return DriverManager.getConnection(banco,usuario, senha );
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
