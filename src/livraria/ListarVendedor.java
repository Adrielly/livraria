package livraria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ListarVendedor {

    public void listarVendedor(JList listaVendedor) {
        try {
            listaVendedor.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.ObterConexao();
            String SQL = "Select*FROM sc_adrielly.vendedor";
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("nome"));
            }
            listaVendedor.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void detalharVendedor(JTable tabela) {
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel) tabela.getModel();
            Connection c = Conexao.ObterConexao();
            String SQL = "Select * FROM sc_adrielly.vendedor";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                
                dtm.addRow(new Object[]{rs.getString("nome"), rs.getString("email"),rs.getString("CPF")});
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
