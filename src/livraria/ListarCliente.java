package livraria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ListarCliente {

    public void listarCliente(JList listaCliente) {
        try {
            listaCliente.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.ObterConexao();
            String SQL = "Select*FROM sc_adrielly.cliente";
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("nome"));
            }
            listaCliente.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void detalharCliente(JTable tabela) {
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel) tabela.getModel();
            Connection c = Conexao.ObterConexao();
            String SQL = "Select * FROM sc_adrielly.cliente";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                
                //dtm.addElement
                dtm.addRow(new Object[]{rs.getString("nome"), rs.getString("email"),rs.getString("CPF")});
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void selecaoGenero (JComboBox combo) {
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.ObterConexao(); 
            PreparedStatement p = c.prepareStatement ("SELECT * FROM sc_adrielly.livro_escolhido");
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
            CadastroGenero a = new CadastroGenero (rs.getString("nomedolivro"));
            System.out.println(a);
            m.addElement(a);
        }
            combo.setModel(m);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
  }
}
