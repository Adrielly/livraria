package livraria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class InsercaoVendedor {
 public void inserirVendedor (String nome, 
         String telefone, String CPF, String email) {
     try {
        Connection c = Conexao. ObterConexao();
        PreparedStatement ps = c.prepareStatement ("insert into sc_adrielly.vendedor(nome_vend, email_vend, cpf_vend, telefone_vend) values (?,?,?,?)");
         ps.setString(1, nome);
         ps.setString(2, email);
         ps.setString(3, CPF);
         ps.setString(4,telefone);
         ps.executeUpdate();
         c.close();
     } catch (SQLException ex) {
         Logger.getLogger(InsercaoCliente.class.getName()).log(Level.SEVERE, null, ex);
     }
        
     
 }
}