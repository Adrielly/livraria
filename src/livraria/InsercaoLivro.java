/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package livraria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class InsercaoLivro {
    
    
    public void livroEscolhido (String nomeDoCliente, String nomeDoLivro, String autorDoLivro,
            String editora, int numPags, String generoDoLivro) throws SQLException {
        PreparedStatement s = null;
        try {
            Connection c = Conexao.ObterConexao();
            String SQL = "insert into sc_adrielly.livro_escolhido (nomeDoLivro, nomeDoAutor, genero, numPags, editoria, cpf_cli) values (?,?,?,?,?,?);";
            s = c.prepareStatement(SQL);
            s.setString(1,nomeDoCliente);
            s.setString(2,nomeDoLivro);
            s.setString(3,autorDoLivro);
            s.setString(4,editora);
            s.setInt(5,numPags);
            s.setString(6,generoDoLivro);
            s.executeUpdate();
            try {
                c.close();
            } catch (SQLException ex) {
                Logger.getLogger(InsercaoLivro.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InsercaoLivro.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            s.close();
        }
        
        
        
    }

    
    
}
