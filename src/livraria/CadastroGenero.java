
package livraria;




public class CadastroGenero {
    private String genero;

    public CadastroGenero(String genero) {
        this.genero = genero;
    }
  
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return genero;
    }
    
    
    
}
